# IPFS transport for APT

## Requirements

```
sudo apt install python3-pip git
sudo pip3 install ipfshttpclient
sudo snap install ipfs
ipfs init
ipfs daemon &
```

You can install IPFS from the snap package, or following the guide at [https://docs.ipfs.io/guides/guides/install/](https://docs.ipfs.io/guides/guides/install/)

You can also install all of the above, except for git, using the requirements file in this repository, with pip3:

`pip3 install -r requirements.txt`

## Install

Copy the ipfs file from this repo to the directory for apt transport methods:

```
git clone https://nest.parrotsec.org/parrot-packages/apt-transport-ipfs.git
cd apt-transport-ipfs
sudo cp ipfs /usr/lib/apt/methods/ipfs
```

## Configure

Add an IPFS mirror to your apt sources.list file. TODO set up a mirror.

## Test it!

You can test it directly with ParrotSec OS repository IPFS mirror!

Backup your /etc/apt/sources.list.d/parrot.list, and comment the already used https mirrors.

Instead of the https mirrors, put this at the end of the file:

```
# ParrotSec Official IPFS mirror, through IPNS name

deb ipfs:/ipns/QmUhEJjvaYWbxnGUD2Y5UsVrNNZHqpftEsq2QhQaKBgnqZ/parrot/ rolling main contrib non-free
deb ipfs:/ipns/QmUhEJjvaYWbxnGUD2Y5UsVrNNZHqpftEsq2QhQaKBgnqZ/parrot/ rolling-security main contrib non-free
```

To test it, run:

```
sudo apt update
sudo apt full-upgrade
```

Made with :rainbow: by JáquerEspeis

Updated by mibofra to use the latest ipfshttpclient